package com.example.demo;

import java.util.Date;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author fabio.sgroi
 */
@RestController
public class HelloRestController {

    @GetMapping("/")
    public String home() {
        return "Spring is here [" + (new Date()) + "]";
    }

    @GetMapping("/hello")
    public String hello() {
        return "HELLO route [" + (new Date()) + "]";
    }
    
    @GetMapping("/test")
    public String test() {
        return "TEST route [" + (new Date()) + "]";
    }
}
